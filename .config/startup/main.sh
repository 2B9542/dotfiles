source $HOME/.config/startup/screen-layout.sh &
source $HOME/.config/startup/set-background.sh &
source $HOME/.config/startup/set-live-wallpaper.sh &

source $HOME/.config/startup/disable-mouse-accel.sh &
source $HOME/.config/startup/disable-dpms.sh &

source $HOME/.config/startup/unmute.sh
source $HOME/.config/startup/greeting.sh &
