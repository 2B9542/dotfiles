monitor_count=$(xrandr | grep -E " connected " | wc -l)

if [ $monitor_count == 1 ]; then
    xrandr --output HDMI-0 --auto
    exit
elif [ $monitor_count == 2 ]; then
    xrandr --output DP-1 --auto --left-of HDMI-0
    exit
fi

