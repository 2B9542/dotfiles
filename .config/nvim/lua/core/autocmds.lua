-- uncomment this if you want to open nvim with a dir
-- vim.cmd [[ autocmd BufEnter * if &buftype != "terminal" | lcd %:p:h | endif ]]

-- Use relative & absolute line numbers in 'n' & 'i' modes respectively
-- vim.cmd[[ au InsertEnter * set norelativenumber ]]
-- vim.cmd[[ au InsertLeave * set relativenumber ]]

-- Don't show any numbers inside terminals
vim.cmd [[ au TermOpen term://* setlocal nonumber norelativenumber | setfiletype terminal ]]

-- Don't show status line on certain windows
vim.cmd [[ autocmd BufEnter,BufRead,BufWinEnter,FileType,WinEnter * lua require("core.utils").hide_statusline() ]]

-- Format ts and js files
--vim.cmd [[ autocmd BufWritePost *.js,*.ts silent! !prettierme <afile> >/dev/null 2>&1 ]]
--vim.cmd [[ autocmd BufWritePost *.js,*.ts silent! Neoformat ]]
--vim.cmd [[ autocmd BufWritePost *.c silent! Neoformat ]]

vim.cmd [[ command! Reload %d|r|1d ]]

-- vim.cmd [[
--   let g:neoformat_javascript_prettier = {
--       \ 'exe': 'prettierd',
--       \ 'args': [bufname()],
--       \ 'replace': 1,
--       \ 'stdin': 1
--       \ }
-- ]]
--
-- Open a file from its last left off position
-- vim.cmd [[ au BufReadPost * if expand('%:p') !~# '\m/\.git/' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif ]]
-- File extension specific tabbing
-- vim.cmd [[ autocmd Filetype python setlocal expandtab tabstop=4 shiftwidth=4 softtabstop=4 ]]
