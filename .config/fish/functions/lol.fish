function lol --description 'toggles setting for bypassing League of Legends anti-cheat'
    set can_play (sysctl abi.vsyscall32 | grep 0 | wc -l)
    if test $can_play -eq 1
        sudo sh -c 'sysctl -w abi.vsyscall32=1 > /dev/null'
        echo "Disabled anti-cheat bypass"
    else
        sudo sh -c 'sysctl -w abi.vsyscall32=0 > /dev/null'
        echo "Enabled anti-cheat bypass"
    end
end
