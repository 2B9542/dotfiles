# Defined via `source`
function clipf --description 'copies a file to the clipboard'
    set filename $argv[1]

    switch $filename
        case "*/*"
            set file_path $filename
        case "*"
            set file_path "$PWD/$filename"
    end

    echo "file://$file_path" | xclip -i -sel clip -target text/uri-list
end
