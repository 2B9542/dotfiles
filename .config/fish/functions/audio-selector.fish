# Defined via `source`
function audio-selector --description 'Prompts user to select an audio file'
    set audio_path /mnt/Xtra/ardour/sessions/memories-of-sand/export

    set argc (count $argv)
    set exec ""

    if test $argc = 0
        set exec "fish -c 'clipf {}'"
    else
        if test $argv[1] = play
            set exec "mpv {} &> /dev/null"
        end
    end

    sed -i 's/directory:.*;/directory: "$audio_path";/g' ~/.config/rofi/config.rasi

    rofi -modi filebrowser \
        -show filebrowser \
        -display-filebrowser "Select audio:" \
        -run-command "fish -c \"echo {cmd} | cut -d' ' -f2 | xargs -I{} $exec\""
end
