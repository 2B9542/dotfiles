function vc --description 'version control helper'
    set current_branch (git rev-parse --abbrev-ref HEAD)

    if test $argv[1] = "new"
        echo "create new branch"
    end

    if test $argv[1] = "merge"
        if test (count $argv) = 1
            set_color red
            echo (set_color red)"[FAILED]" (set_color normal)"pass in the branch to merge into"(set_color blue) $current_branch
            exit 1
        end

        set remote_branch $argv[2]

        # TODO: check for status code
        git checkout $remote_branch && git pull && git checkout $current_branch && git merge --no-edit && git push -u origin $current_branch && echo (set_color green)"[MERGED]" (set_color normal)"$remote_branch into"(set_color blue) "$current_branch"
    end
    
    if test $argv[1] = "branch"
        if test (count $argv) = 1
            set_color red
            echo (set_color red)"[FAILED]" (set_color normal)"pass in the environment" (set_color blue)"(develop, innovate, demo)"
            echo (set_color normal)"e.g. vc branch demo"
            exit 1
        end

        set _environment "$argv[2]"
        set new_branch "$current_branch-$_environment"

        git checkout -b $new_branch && vc merge $_environment && git checkout $current_branch && echo (set_color green)"[CREATED]" (set_color normal)"$new_branch"
    end

    if test $argv[1] = "help"
        set command $argv[2]

        if test $command = "branch"
            echo "Description: create new branch for a specific environment"
            echo (set_color normal)"    vc branch"(set_color green) "demo"
            echo (set_color blue)"    hotfix/IN-0000 ->"(set_color green) "hotfix/IN-0000-demo"
        end
    end
end
