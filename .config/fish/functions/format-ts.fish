
function format-ts --description 'formats *.ts and *.js files'
    set PORT (cat ~/.prettierd | cut -d" " -f1)
    set TOKEN (cat ~/.prettierd | cut -d" " -f2)
    echo "$TOKEN $PWD --stdin" | cat - $argv[1] | ncat localhost $PORT
end
