# Defined via `source`
function select-audio --description 'Prompts user to select an audio file'
  set audio_path "/mnt/Xtra/ardour/sessions/memories-of-sand/export"
  set files (fd -t file . "$audio_path")

  set selection (echo $files | xargs -d " " -I{} echo {} | rev | cut -d'/' -f1 | rev | rofi -show drun -dmenu -p "Select audio:" | xargs -I{} echo {})

  clipf (echo $files | xargs -d " " -I{} echo {} | rg "$selection")
end
