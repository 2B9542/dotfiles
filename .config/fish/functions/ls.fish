# Defined via `source`
function ls --wraps='exa ' --description 'shows all files using exa instead of ls'
  exa -a --color=always $argv; 
end

function ll --wraps='exa ' --description 'shows all files using exa instead of ls'
  exa -ahlg --color=always --icons $argv; 
end
