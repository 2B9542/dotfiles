# Defined via `source`
function config --wraps='git --git-dir=/mnt/Xtra/.dotfiles/ --work-tree=/home/tony ' --description 'execute git commands in .dotfiles repository'
  git --git-dir=/mnt/Xtra/.dotfiles/ --work-tree=/home/tony  $argv; 
end
