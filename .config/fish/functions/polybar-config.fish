# Defined via `source`
function polybar-config --wraps='nvim ~/.config/polybar/config ' --description 'alias polybar-config nvim ~/.config/polybar/config '
  nvim ~/.config/polybar/config  $argv; 
end
