function mpv --description 'alias mpv mpv --msg-level=all=error'
    command mpv --msg-level=all=error $argv
end
