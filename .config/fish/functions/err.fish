# Defined via `source`
function err --description 'handle errors'
    set audio_path /mnt/Xtra/ardour/sessions/memories-of-sand/export

    set playing false

    cat /dev/stdin | while read line
        if test $line != ""
            echo (set_color red) $line
            if test $playing = false
                fish -c "mpv $audio_path/concerns.wav &>/dev/null" &
                set playing true
            end
        end
    end
end
