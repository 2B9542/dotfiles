function tenor --description 'search gifs on tenor.com'
    set query (rofi -dmenu -p "gif search")

    if test -z query
        echo "empty"
        exit
    end

    brave "https://tenor.com/search/$query"
end

