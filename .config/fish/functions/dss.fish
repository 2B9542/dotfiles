# Defined via `source`
function dss --description 'screenshot tool with copy to clipboard'
    xfce4-screenshooter -m -r -s /tmp/ss.png && clipf /tmp/ss.png
end
