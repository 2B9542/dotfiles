# Defined via `source`
function wallpaper --description 'set animated wallpaper'
    if test (count $argv) = 0
        exit 1
    end

    killall xwinwrap

    sleep 0.3

    xwinwrap -ov -g 1920x1080+0+0 -- mpv -wid WID "$argv[1]" --no-osc --no-osd-bar --loop-file --player-operation-mode=cplayer --panscan=1.0 --no-keepaspect --no-input-default-bindings --hwdec & disown
end
