# Defined via `source`
function i3-config --wraps='nvim ~/.config/i3/config ' --description 'alias i3-config nvim ~/.config/i3/config '
  nvim ~/.config/i3/config  $argv; 
end
