# Defined via `source`
function alacritty-config --wraps='nvim ~/.config/alacritty/alacritty.yml ' --description 'alias alacritty-config nvim ~/.config/alacritty/alacritty.yml '
  nvim ~/.config/alacritty/alacritty.yml  $argv; 
end
